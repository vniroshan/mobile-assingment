package com.niroshan.android_unit_testing_assignment

class Calculation {
    fun addTwoNumbers(a: Double, b: Double): Double {
        return a + b
    }

    fun subtractTwoNumbers(a: Double, b: Double): Double {
        return a - b
    }

    fun multiplyTwoNumbers(a: Double, b: Double): Double {
        return a * b
    }

    fun divideTwoNumbers(a: Double, b: Double): Double {
        return a / b
    }
}