package com.niroshan.android_unit_testing_assignment

import junit.framework.TestCase
import org.junit.Test

class CalculationTest : TestCase() {

    @Test
    fun testAddTwoNumbersPositiveNumbers() {
        assertEquals(8.0, Calculation().addTwoNumbers(5.0, 3.0), 0.0)
    }
    @Test
    fun testAddTwoNumbersNegativeNumbers() {
        assertEquals(-8.0, Calculation().addTwoNumbers(-5.0, -3.0), 0.0)
    }

    @Test
    fun testAddTwoNumbersPositiveAndNegativeNumbers() {
        assertEquals(-2.0, Calculation().addTwoNumbers(-5.0, 3.0), 0.0)
    }

    fun testSubtractTwoNumbers() {}

    fun testMultiplyTwoNumbers() {}

    fun testDivideTwoNumbers() {}
}